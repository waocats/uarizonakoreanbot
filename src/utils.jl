@Base.kwdef struct Item
    kr_word::String
    eng_senses::Vector{Pair{String, String}}
    pos::String
end


function load_dictionary()
    if isdir(DICT_DIR) && isfile("$DICT_DIR/dict.jld2")
        return load("$DICT_DIR/dict.jld2", "d")
    elseif isdir(XML_DIR) && !isempty(readdir(XML_DIR))
        return generate_dictionary()
    else
        return Dict()
    end
end

function save_dictionary(d)
    !isdir(DICT_DIR) && mkpath(DICT_DIR)
    save("$DICT_DIR/dict.jld2", "d", d)
end

function generate_dictionary()
    wfiles = readdir(XML_DIR)
    return Dict(getindex.(split.(wfiles, ".xml"), 1) .=> get_items.(joinpath.(XML_DIR, wfiles)))
end

# Helper functions for translations
hastranslation(sense) = haskey(sense, "translation") && sense["translation"]["trans_word"] isa String && sense["translation"]["trans_dfn"] isa String
translationpair(sense) = (sense["translation"]["trans_word"] => sense["translation"]["trans_dfn"])

function get_items(wfile)
    data = xml_dict(replace(read(wfile, String), r"\n|\t|\r" => ""), Dict)["channel"]

    items = []
    if haskey(data, "item")
        !(data["item"] isa Vector) && (data["item"] = tuple(data["item"]))
        for item in data["item"]
            # Handle part-of-speech
            if item["pos"] isa String
                pos = item["pos"]
            else
                pos = "?"
            end

            # Handle senses
            eng_senses = []
            if haskey(item, "sense")
                # Add translation(s) to eng_senses
                if item["sense"] isa Vector
                    append!(eng_senses, translationpair(sense) for sense in item["sense"] if hastranslation(sense))
                end
                if item["sense"] isa Dict
                    hastranslation(item["sense"]) && push!(eng_senses, translationpair(item["sense"]))
                end
                
                # Create Item with word, part-of-speech, and sense and add it to the list of Items
                !isempty(eng_senses) && push!(items, Item(kr_word=item["word"], eng_senses=eng_senses, pos=pos))
            end
        end
    end
    return items
end

function lookup!(d, w)
    if !haskey(d, w)
        !isdir(XML_DIR) && mkpath(XML_DIR)
        wfile = "$XML_DIR/$w.xml"
        if !isfile(wfile)
            request("https://krdict.korean.go.kr/api/search?key=$KRDICT_API_KEY&q=$w&translated=y&trans_lang=1", output=wfile)
        end
        
        d[w] = get_items(wfile)
    end
    return d[w]
end

function format_results(items)
    results = "```md"
    for item in items
        results_to_add = "\n# $(item.kr_word) [$(item.pos)]"
        for i in 1:length(item.eng_senses)
            results_to_add *= "\n * $(item.eng_senses[i][1]) — $(item.eng_senses[i][2])\n"
            if length(results_to_add) + length(results) <= 1997
                results *= results_to_add
            else
                break
            end
        end
    end
    results *= "```"
    
    return results
end
